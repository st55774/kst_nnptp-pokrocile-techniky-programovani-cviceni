package cz.upce.fei.entity;

import java.util.Objects;

public class Entita implements Comparable<Entita>{
    private final int id;

    public Entita(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Entita entita = (Entita) o;
        return id == entita.id;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

    @Override
    public int compareTo(Entita otherEntity) {
        return id - otherEntity.getId();
    }
}
