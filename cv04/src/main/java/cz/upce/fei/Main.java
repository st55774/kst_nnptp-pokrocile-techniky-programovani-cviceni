package cz.upce.fei;

import cz.upce.fei.Structures.EntitaSeznam;
import cz.upce.fei.Structures.Zasobnik;
import cz.upce.fei.Structures.ZasobnikEnum;
import cz.upce.fei.Structures.ZasobnikImpl;
import cz.upce.fei.entity.Entita;
import cz.upce.fei.entity.Uzivatel;

public class Main {
    public enum MODELS{
        DOPE0X,
        LITTLE_CAPRI,
        MALA_NATALKA,
        ANDREA_KALOUSOVA
    }

    public static void main(String[] args) {
        Zasobnik zasobnik = new ZasobnikImpl(Integer.class);

        zasobnik.vloz(12);
        zasobnik.vloz(15);
        System.out.println(zasobnik.odeber());

        Zasobnik<String> strZasobnik = new ZasobnikImpl<>(String.class);
        strZasobnik.vloz("Ahoj");
        strZasobnik.vloz("Cau");
        strZasobnik.vloz("Zdur");

        String toFind = "Ahoj";

        System.out.println(strZasobnik.najdi(toFind::startsWith));

        Zasobnik<MODELS> modelsZasobnik = new ZasobnikEnum<>(MODELS.class);
        modelsZasobnik.vloz(MODELS.DOPE0X);
        modelsZasobnik.vloz(MODELS.LITTLE_CAPRI);
        modelsZasobnik.vloz(MODELS.MALA_NATALKA);
        modelsZasobnik.vloz(MODELS.ANDREA_KALOUSOVA);

        EntitaSeznam<Uzivatel> seznamEntity = new EntitaSeznam<>(Uzivatel.class);
        seznamEntity.vloz(new Uzivatel(12));
        seznamEntity.vloz(new Uzivatel(1));
        seznamEntity.vloz(new Uzivatel(8));
        seznamEntity.vloz(new Uzivatel(2));

        seznamEntity.serad();

        printIds(seznamEntity);
    }

    public static <T extends Entita> void printIds(EntitaSeznam<T> entitaSeznam){
        entitaSeznam.forEach(System.out::println);
    }
}
