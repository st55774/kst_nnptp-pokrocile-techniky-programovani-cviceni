package cz.upce.fei.Structures;

import java.util.function.Predicate;

public interface Zasobnik<T> {
    void vloz(T item);

    T odeber();

    T najdi(Predicate<T> predicate);
}
