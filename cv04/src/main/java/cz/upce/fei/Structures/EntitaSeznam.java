package cz.upce.fei.Structures;

import cz.upce.fei.entity.Entita;

import java.lang.reflect.Array;
import java.util.Arrays;
import java.util.function.Consumer;

public class EntitaSeznam<T extends Entita> extends ZasobnikImpl<T> {
    public EntitaSeznam(Class<T> dataType) {
        super(dataType);
    }

    public T najdiId(int id){
        return super.najdi(entity -> entity.getId() == id);
    }

    public void serad(){
        Arrays.sort(super.objects, 0, super.getCurrentIndex());
    }

    public void forEach(Consumer<T> consumer){
        Arrays.stream(super.objects).forEach(consumer);
    }
}
