package cz.upce.fei.Structures;

import java.util.function.Predicate;

public class ZasobnikEnum<T extends Enum<T>> extends ZasobnikImpl<T>{
    public ZasobnikEnum(Class<T> dataType) {
        super(dataType);
    }
}
