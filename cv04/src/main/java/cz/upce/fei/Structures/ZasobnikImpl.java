package cz.upce.fei.Structures;

import java.lang.reflect.Array;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.function.Predicate;

public class ZasobnikImpl<T> implements Zasobnik<T> {
    private static final int DEFAULT_SIZE = 10;

    private static final int MULTIPLICATION = 2;

    private int currentIndex = 0;

    protected T[] objects;

    private final Class<T> dataType;

    private static Map<Class, Integer> instanceCounter = new HashMap<>();

    public ZasobnikImpl(Class<T> dataType) {
        this.dataType = dataType;
        objects = (T[]) Array.newInstance(dataType, DEFAULT_SIZE);
    }

    @Override
    public void vloz(T item){
        if(currentIndex == objects.length)
            resizeArray();

        objects[currentIndex] = item;

        currentIndex++;
        increment(dataType);
    }

    @Override
    public T odeber(){
        if(currentIndex <= 0)
            throw new ArrayIndexOutOfBoundsException();

        currentIndex--;

        decrement(dataType);
        return objects[currentIndex];
    }

    @Override
    public T najdi(Predicate<T> predicate) {
        for (T item : objects)
            if(predicate.test(item)) return item;
        return null;
    }

    private void resizeArray(){
        int nextSize = objects.length * MULTIPLICATION;

        T[] objects = (T[]) Array.newInstance(dataType, nextSize);
        System.arraycopy(this.objects, 0, objects, 0, this.objects.length);

        this.objects = objects;
    }

    private static <T> int increment(Class<T> classToIncrement){
        if(!instanceCounter.containsKey(classToIncrement))
            instanceCounter.put(classToIncrement, 0);

        Integer count = instanceCounter.get(classToIncrement) + 1;
        instanceCounter.replace(classToIncrement, count);

        return count;
    }

    private static <T> int decrement(Class<T> classToDecrement){
        if(!instanceCounter.containsKey(classToDecrement) || instanceCounter.get(classToDecrement) <= 0)
            throw new NoSuchElementException();

        Integer count = instanceCounter.get(classToDecrement) - 1;
        instanceCounter.replace(classToDecrement, count);

        return count;
    }

    protected int getCurrentIndex() {
        return currentIndex;
    }
}
