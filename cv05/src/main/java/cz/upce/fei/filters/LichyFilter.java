package cz.upce.fei.filters;

import java.util.function.IntPredicate;

public class LichyFilter implements IntPredicate {
    public LichyFilter() {
    }

    @Override
    public boolean test(int value) {
        return value % 2 != 0;
    }
}