package cz.upce.fei.entity;

public class OsobaDite {
    private String OsobaJmeno;
    private String DiteJmeno;

    public OsobaDite(String osobaJmeno, String diteJmeno) {
        OsobaJmeno = osobaJmeno;
        DiteJmeno = diteJmeno;
    }

    public String getOsobaJmeno() {
        return OsobaJmeno;
    }

    public void setOsobaJmeno(String osobaJmeno) {
        OsobaJmeno = osobaJmeno;
    }

    public String getDiteJmeno() {
        return DiteJmeno;
    }

    public void setDiteJmeno(String diteJmeno) {
        DiteJmeno = diteJmeno;
    }

    @Override
    public String toString() {
        return OsobaJmeno + ' ' + DiteJmeno;
    }
}
