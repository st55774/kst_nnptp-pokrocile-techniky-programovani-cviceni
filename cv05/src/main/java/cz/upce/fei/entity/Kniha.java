package cz.upce.fei.entity;

public class Kniha {
    public int vydavatelCislo;
    public String nazev;
    public String autor;

    public Kniha(String autor, String nazev, int vydavatelCislo) {
        this.vydavatelCislo = vydavatelCislo;
        this.nazev = nazev;
        this.autor = autor;
    }

    @Override
    public String toString() {
        return "Kniha{" + "vydavatelCislo=" + vydavatelCislo + ", nazev="
                + nazev + ", autor=" + autor + '}';
    }
}