package cz.upce.fei.entity;

public class Vydavatel {
    public String nazev;
    public int cislo;

    public Vydavatel(String nazev, int cislo) {
        this.nazev = nazev;
        this.cislo = cislo;
    }

    @Override
    public String toString() {
        return "Vydavatel{" + "nazev=" + nazev + ", cislo=" + cislo +
                '}';
    }
}