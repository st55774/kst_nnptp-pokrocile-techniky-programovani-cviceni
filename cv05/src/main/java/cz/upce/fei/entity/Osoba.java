package cz.upce.fei.entity;

import java.util.List;

public class Osoba{
    private String jmeno;
    private String prijmeni;
    private int vek;
    private List<String> deti;

    public Osoba(String jmeno, String prijmeni) {
        this.jmeno = jmeno;
        this.prijmeni = prijmeni;
    }

    public Osoba(String jmeno, String prijmeni, int vek) {
        this.jmeno = jmeno;
        this.prijmeni = prijmeni;
        this.vek = vek;
    }

    public Osoba(String jmeno, String prijmeni, int vek, List<String> deti) {
        this.jmeno = jmeno;
        this.prijmeni = prijmeni;
        this.vek = vek;
        this.deti = deti;
    }

    public String getJmeno() {
        return jmeno;
    }

    public void setJmeno(String jmeno) {
        this.jmeno = jmeno;
    }

    public String getPrijmeni() {
        return prijmeni;
    }

    public void setPrijmeni(String prijmeni) {
        this.prijmeni = prijmeni;
    }

    public int getVek() {
        return vek;
    }

    public void setVek(int vek) {
        this.vek = vek;
    }

    public List<String> getDeti() {
        return deti;
    }

    public void setDeti(List<String> deti) {
        this.deti = deti;
    }

    public String jmeno(){
        return String.format("%s %s", jmeno, prijmeni);
    }

    @Override
    public String toString() {
        return "Osoba{" +
                "jmeno='" + jmeno + '\'' +
                ", prijmeni='" + prijmeni + '\'' +
                ", vek=" + vek +
                ", deti=" + deti +
                '}';
    }
}
