package cz.upce.fei.entity;

import java.util.List;

public class KnihaVydavatel {
    private Vydavatel vydavatel;
    private List<Kniha> knihy;

    public KnihaVydavatel(Vydavatel vydavatel, List<Kniha> knihy) {
        this.vydavatel = vydavatel;
        this.knihy = knihy;
    }

    public Vydavatel getVydavatel() {
        return vydavatel;
    }

    public void setVydavatel(Vydavatel vydavatel) {
        this.vydavatel = vydavatel;
    }

    public List<Kniha> getKnihy() {
        return knihy;
    }

    public void setKnihy(List<Kniha> knihy) {
        this.knihy = knihy;
    }

    @Override
    public String toString() {
        return "KnihaVydavatel{" +
                "vydavatel=" + vydavatel +
                ", kniha=" + knihy +
                '}';
    }
}
