package cz.upce.fei;

import cz.upce.fei.entity.*;
import cz.upce.fei.filters.LichyFilter;

import java.util.Arrays;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class App {
    private static final int[] cisla = {5, 4, 1, 3, 9, 8, 6, 7, 2, 0};

    private static Osoba[] osoby = new Osoba[]{
            new Osoba("Roman", "Prymula", 18, Arrays.asList("Bartos", "Vojtech")),
            new Osoba("Roman Noční", "Prymula", 21, Arrays.asList("Dada", "Fiala")),
            new Osoba("Pitomio", "OčiSloupa", 28, Arrays.asList("Japan", "Thaiwan")),
            new Osoba("Andrej", "Bureš", 50, Arrays.asList("Psychous", "Capi hnizdo")),
    };

    public static void main(String[] args) {
        printQuest03();
        printQuest04();
        printQuest05();
        printQuest06();
        printQuest0708();
        printQuest09();
        printQuest01();
    }

    private static void printQuest01() {
        Vydavatel[] vydavatele = new Vydavatel[]{
                new Vydavatel("Computer Press", 1),
                new Vydavatel("Grada", 2),
                new Vydavatel("Academia", 3)};
        Kniha[] knihy = new Kniha[]{
                new Kniha("Pecinovský", "Word 2003", 1),
                new Kniha("Virius", "Jazyk C#", 2),
                new Kniha("Prata", "Mistrovství v C++", 1),
                new Kniha("Topfer", "Datove struktury", 2)};

        Arrays.stream(vydavatele)
              .map(vydavatel -> {
                  List<Kniha> knihyVydavatele = Arrays
                          .stream(knihy)
                          .filter(kniha -> kniha.vydavatelCislo == vydavatel.cislo)
                          .collect(Collectors.toList());
                  return new KnihaVydavatel(vydavatel, knihyVydavatele);
              })
              .filter(knihaVydavatel -> knihaVydavatel.getKnihy().size() != 0)
              .forEach(System.out::println);
    }

    private static void printQuest09() {
        Arrays.stream(osoby)
              .sorted(Comparator.comparingInt(Osoba::getVek))
              .collect(Collectors.groupingBy(osoba -> Math.ceil(osoba.getVek()/10.0)))
              .forEach((vek, osoba) -> {
                  System.out.printf("Vek %.0f až %.0f:\n", (vek-1)*10, vek*10);
                  osoba.forEach(System.out::println);
              });
    }

    private static void printQuest0708() {
        Arrays.stream(osoby)
              .flatMap(osoba -> osoba.getDeti()
                      .stream()
                      .map(dite -> new OsobaDite(osoba.jmeno(), dite)))
              .collect(Collectors.toList())
              .forEach(System.out::println);
    }

    private static void printQuest06() {
        Arrays.stream(osoby)
           .filter(osoba -> osoba.getVek() > 20)
           .sorted((o1, o2) -> {
                int difference = o1.getPrijmeni().compareTo(o2.getPrijmeni());
                return (difference != 0)? difference : o1.getJmeno().compareTo(o2.getJmeno());
            })
           .forEach(System.out::println);

        System.out.println();
    }

    private static void printQuest05() {
        Arrays.stream(osoby).sorted((o1, o2) -> {
            int difference = o1.getPrijmeni().compareTo(o2.getPrijmeni());
            return (difference != 0)? difference : o1.getJmeno().compareTo(o2.getJmeno());
        }).forEach(System.out::println);

        System.out.println();
    }

    private static void printQuest03() {
        System.out.println(Arrays.stream(cisla).min().getAsInt());
        System.out.println(Arrays.stream(cisla).filter(cislo -> cislo % 2 == 0).count());
        System.out.println(Arrays.stream(cisla).filter(cislo -> cislo % 2 == 0).max().getAsInt());
    }

    private static void printQuest04() {
        Vypis(Arrays.stream(cisla));
        VypisOpozdene(Arrays.stream(cisla));
    }

    /**
     * https://stackoverflow.com/questions/29216588/how-to-ensure-order-of-processing-in-java8-streams
     * */
    private static void Vypis(IntStream cisla){
        cisla.filter(cislo -> cislo % 2 != 0)
                .forEach(cislo -> System.out.printf("%d ", cislo));
        System.out.println();
    }

    /**
     * https://stackoverflow.com/questions/29915591/java-8-stream-operations-execution-order
     * https://stackoverflow.com/questions/52761108/stateful-filter-for-ordered-stream
     * */
    private static void VypisOpozdene(IntStream cisla){
        cisla.sequential()
                .filter(new LichyFilter())
                .forEach(cislo -> System.out.printf("%d ", cislo));

        System.out.println();
    }
}
