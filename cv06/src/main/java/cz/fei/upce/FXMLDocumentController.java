package cz.fei.upce;

import java.net.URL;
import java.util.ResourceBundle;
import java.util.concurrent.ExecutionException;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.image.ImageView;

public class FXMLDocumentController implements Initializable {

    @FXML
    private Label label;

    @FXML
    private ImageView image;

    @Override
    public void initialize(URL url, ResourceBundle rb) {
    }

    private AlgorithmBase createAlgorithm() {
        return new Mandelbrot();
    }

    @FXML
    private void handleButtonSync(ActionEvent event) {
        AlgorithmBase ns = createAlgorithm();
        ns.startTimeMeasurement();
        ns.solve();
        ns.stopTimeMeasurement();

        image.setImage(ns.getImageFX());
        label.setText(Double.toString(ns.getDuration()));
    }

    @FXML
    private void handleButtonCompletableFuture(ActionEvent event) throws ExecutionException, InterruptedException {
        // ...
        // TODO: solve problem using Thread class (thread for each image row)
        // TODO: solve problem using Thread class (thread for each processor)
        // TODO: solve problem using thread pool
        // TODO: solve problem using completable futures (try different ways to do it)
        // IMPORTANT: be aware of synchronization issues and blocking of UI thread

        AlgorithmBase ns = createAlgorithm();
        ns.startTimeMeasurement();
        ns.solveRowsBetterCF();
        ns.stopTimeMeasurement();

        image.setImage(ns.getImageFX());
        label.setText(Double.toString(ns.getDuration()));

    }

    @FXML
    private void handleButtonCompletableFutureRows(ActionEvent event) throws InterruptedException, ExecutionException {
        AlgorithmBase ns = createAlgorithm();
        ns.startTimeMeasurement();
        ns.solveRowsBetter();
        ns.stopTimeMeasurement();

        image.setImage(ns.getImageFX());
        label.setText(Double.toString(ns.getDuration()));
    }

}
