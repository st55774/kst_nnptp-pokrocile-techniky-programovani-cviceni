package cz.fei.upce;

import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.*;

import javafx.embed.swing.SwingFXUtils;
import javafx.scene.image.WritableImage;

public abstract class AlgorithmBase {

    protected int width = 600;
    protected int height = 600;

    protected long startTime;
    protected long stopTime;

    protected final BufferedImage image;

    protected ThreadPoolExecutor threadPoolExecutor = (ThreadPoolExecutor) Executors.newFixedThreadPool(180);

    public AlgorithmBase() {
        image = new BufferedImage(width, height, BufferedImage.TYPE_INT_ARGB);
    }

    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public BufferedImage getImage() {
        return image;
    }

    public WritableImage getImageFX() {
        return SwingFXUtils.toFXImage(getImage(), null);
    }

    public void startTimeMeasurement() {
        startTime = System.currentTimeMillis();
    }

    public void stopTimeMeasurement() {
        stopTime = System.currentTimeMillis();
    }

    public double getDuration() {
        double tmp = (stopTime - startTime) / 1000.0;
        return Math.round(tmp * 1000) / 1000.0;
    }

    public void solve() {
        for (int i = 0; i < height; i++) {
            for (int j = 0; j < width; j++) {
                solvePoint(j, i);
            }
        }
    }

    public void solveRowsBetter() throws InterruptedException, ExecutionException {
        Collection<Future<?>> futures = new LinkedList<>();

        for (int i = 0; i < height; i++) {
            final int[] coordinates = {i};
            var future = threadPoolExecutor.submit(() -> {
                for (int j = 0; j < width; j++) {
                    solvePoint(j, coordinates[0]);
                }
            });
            futures.add(future);
        }

        for (var future : futures)
            future.get();  //Thread.join
    }

    public void solveRowsBetterCF() throws InterruptedException, ExecutionException {
        List<CompletableFuture<Void>> pool = new ArrayList<>();

        for (int i = 0; i < height; i++) {
            final int[] coordinates = {i};
            var future = CompletableFuture.runAsync(() -> {
                for (int j = 0; j < width; j++) {
                    solvePoint(j, coordinates[0]);
                }
            });
            pool.add(future);
        }

        CompletableFuture<Void>[] array = new CompletableFuture[pool.size()];
        //pool.forEach(future -> {  });

        var finalFuture = CompletableFuture.allOf((pool.toArray(array)));
        finalFuture.get();
    }

    public void solveRow(int row) {
        int i = row;
        for (int j = 0; j < width; j++) {
            solvePoint(j, i);
        }
    }

    public void solvePart(int part, int totalNumOfParts) {
        int w = getHeight() / totalNumOfParts;
        int row = part * w;

        for (int i = 0; i < w; i++) {
            for (int j = 0; j < getWidth(); j++) {
                solvePoint(j, row);
            }
            row++;
        }
    }

    public abstract void solvePoint(int j, int i);

}
