package cz.fei.upce;

import cz.fei.upce.gui.GuiApplication;
import javafx.application.Application;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * https://openjfx.io/openjfx-docs/#gradle
 * https://blog.jetbrains.com/idea/2019/11/tutorial-reactive-spring-boot-a-javafx-spring-boot-application/?gclid=CjwKCAjw5p_8BRBUEiwAPpJO6yJ4onCIAvdgY_aCzvAnytTkJ6jumi8oarDk8eCc50vgCFZNN2FrshoCeMcQAvD_BwE
 * https://spring.io/guides/gs/spring-boot/
 * https://stackoverflow.com/questions/50231736/applicationcontextexception-unable-to-start-servletwebserverapplicationcontext/50232382
 * */
@SpringBootApplication
public class App{
    public static void main(String[] args) {
        Application.launch(GuiApplication.class, args);
    }
}
